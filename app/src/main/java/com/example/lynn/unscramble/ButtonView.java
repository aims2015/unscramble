package com.example.lynn.unscramble;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import static com.example.lynn.unscramble.MainActivity.buttons;
import static com.example.lynn.unscramble.MainActivity.helper;
import static com.example.lynn.unscramble.MainActivity.listener;
import static com.example.lynn.unscramble.MainActivity.word;

/**
 * Created by lynn on 6/23/2015.
 */
public class ButtonView extends RelativeLayout {

    public String getWord() {
        SQLiteDatabase database = helper.getReadableDatabase();

        ArrayList<String> words = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM words;",new String[]{});

        cursor.moveToFirst();

        do {
            int wordIndex = cursor.getColumnIndex("word");

            String word = cursor.getString(wordIndex);

            words.add(word.toUpperCase());
        } while (cursor.moveToNext());

        String word = words.get((int)(words.size()*Math.random()));


        return(word);
    }

    public String scramble(String input) {
        char[] characters = input.toCharArray();

        for (int counter=0;counter<100;counter++) {
            int position1 = (int)(characters.length*Math.random());

            int position2 = (int)(characters.length*Math.random());

            while (position1 == position2)
                position2 = (int)(characters.length*Math.random());

            char temp = characters[position1];
            characters[position1] = characters[position2];
            characters[position2] = temp;
        }

        return(new String(characters));
    }

    public ButtonView(Context context) {
        super(context);

        setBackgroundColor(0xFF00FF8F);

        word = getWord();

        String scrambledWord = scramble(word);

        buttons = new Button[word.length()];

        for (int counter=0;counter<buttons.length;counter++) {
            buttons[counter] = new Button(context);

            buttons[counter].setText(scrambledWord.substring(counter,counter+1));

            buttons[counter].setOnTouchListener(listener);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100,100);

            buttons[counter].setLayoutParams(layoutParams);

            addView(buttons[counter]);
        }

    }

}
