package com.example.lynn.unscramble;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import static com.example.lynn.unscramble.MainActivity.*;
import static com.example.lynn.unscramble.MainActivity.word;

/**
 * Created by lynn on 6/23/2015.
 */
public class MyView extends RelativeLayout {


    public String check() {
        TreeSet<Button> set = new TreeSet<Button>(new Comparator<Button>() {

            public int compare(Button button1,Button button2) {
                RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams)button1.getLayoutParams();

                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)button2.getLayoutParams();

                if (layoutParams1.leftMargin < layoutParams2.leftMargin)
                    return(-1);
                else if (layoutParams1.leftMargin > layoutParams2.leftMargin)
                    return(1);
                else
                    return(0);

            }

        });


        for (int counter=0;counter<buttons.length;counter++)
            set.add(buttons[counter]);

        String output = "";

        for (Button button : set)
            output += button.getText().toString().toUpperCase();

        return(output);
    }

    public MyView(Context context) {
        super(context);

        submit = new Button(context);

        submit.setText("Submit");

        submit.setId(R.id.submit);

        submit.setOnClickListener(listener);

        addView(submit);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,100);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.submit);

        buttonView = new ButtonView(context);

        buttonView.setLayoutParams(layoutParams);

        addView(buttonView);


    }

}
