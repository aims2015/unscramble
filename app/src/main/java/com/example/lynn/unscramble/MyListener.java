package com.example.lynn.unscramble;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import static com.example.lynn.unscramble.MainActivity.*;

/**
 * Created by lynn on 6/23/2015.
 */
public class MyListener implements View.OnClickListener, View.OnTouchListener {
    private float offsetX;
    private float offsetY;

    @Override
    public void onClick(View v) {
        Toast.makeText(v.getContext(),"word is " + word + " guess is " + myView.check(),Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RelativeLayout.LayoutParams layout = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offsetX = event.getRawX() - layout.leftMargin;
            offsetY = event.getRawY() - layout.topMargin;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            float tempX = event.getRawX() - offsetX;
            float tempY = event.getRawY() - offsetY;

            layout.leftMargin = (int)tempX;
            layout.topMargin = (int)tempY;

            v.setLayoutParams(layout);
        }

        return(true);
    }

}
